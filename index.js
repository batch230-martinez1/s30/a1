// 1
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$count: "fruitsOnSale"}
]);

// 2

db.fruits.aggregate([
  {$match: {stock:{$gt: 20}}},
  {$count: "enoughStock"}
]);

// 3

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
  {$sort: {total: 1}},
]);

// 4

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
  {$sort: {total: 1}},
]);

// 5

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
  {$sort: {total: 1}},
]);
